<h2>Registrácia príspevkov</h2>
<form enctype="multipart/form-data" action="" method="post" name="main">
	<input type="hidden" name="action" id="action" value="none"/>
<?php
	if (isset($_SESSION['newsub']) && $_SESSION['newsub'] > -1) {
?>
		<p class="message">
			Úspešne ste zaregistrovali váš príspevok s registračným číslom <?=$_SESSION['newsub']?>.<br/>
			Ak chcete svoj príspevok zmeniť, kontaktujte nás.<br/>
			Ak chcete pridať ďalší príspevok postupujte stlačením príslušného tlačidla.
		</p>
		<input type="button" class="button" value="Prihlásiť ďalší príspevok" name="clearsubmission" id="clearsubmission" onclick="clearSubmission()"/> 
<?php		
	} else {
?>			
		<fieldset>
			<p class="message">
				<?=($message != "")?$message:"Polia vyznačené hrubým písmom sú povinné."?>
			</p>
<?php
		$lastValues = array();
//		/$submissionFields = array("id", "firstauthor", "email", "otherauthor", "title", "abstract", "stype", "notes");
	foreach ($submissionFields as $name) {
		if ($name == 'id' || $name == 'notes' || $name == 'createdate') {
			;
		}
		elseif ($name == 'stype') {
			$val = (isset($_POST[$name])) ? $_POST[$name] : 2;
		?>			
			<label for="<?=$name?>"><?=$submissionFieldNames[$name]?>:</label>
			<select id="<?=$name?>" name="<?=$name?>">
				<option value="1"<?=($val==1)?' selected="selected"':''?>>Prehľadová prednáška (50 min.)</option>
				<option value="2"<?=($val==2)?' selected="selected"':''?>>Krátka prednáška (20 min.)</option>
				<!--<option value="3"<?=($val==3)?' selected="selected"':''?>>Poster</option>-->
			</select>
			<div class="clear"></div>
		<?php
		}
		elseif ($name == 'abstract') {
		?>
			<label for="<?=$name?>"><strong><?=$submissionFieldNames[$name]?></strong>:<br/>(cca 150 slov)</label>
			<textarea id="<?=$name?>" name="<?=$name?>" rows="10" cols="61"><?=$val?></textarea>
		<?php	
		}
		else {
			$val = (isset($_POST[$name])) ? $_POST[$name] : "";
			$label = $submissionFieldNames[$name];
			if (in_array($name,$submissionFieldsObligatory,true))
				$label = "<strong>".$label."</strong>";
			?>
			<label for="<?=$name?>"><?=$label?>:</label>
			<input type="text" name="<?=$name?>" id="<?=$name?>" value="<?=$val?>"/>
			<div class="clear"></div>
			<?php
		}
	}
?>
			<div class="captchabox">
				<label for="captchain"><strong>Captcha kontrola*:</strong></label>
				<input type="text" name="captchain" id="captchain" value=""/>
                <img src="<?=$_SESSION['captcha']['image_src']?>" alt="CAPTCHA"/>
				<p>*pre dokončenie registrácie príspevku vyplňte napíšte do príslušného políčka znaky na obrázku.</p>
			</div>
			<div class="notesbox">
				<label for="notes">Poznámky*:</label>
				<textarea id="notes" name="notes" rows="5" cols="30"><?=(isset($_POST['note'])?$_POST['note']:"")?></textarea>
				<div class="clear"></div>
				<p>*napíšte nám, či na prezentáciu svojho príspevku potrebujete neštandardnú techniku a podobne</p>
			</div>
			<div class="clear"></div>
			<input type="button" class="button" value="Prihlásiť príspevok" name="submitbutton" id="submittbutton" onclick="addSumbission()"/> 
		</fieldset>
<?php } ?>		
</form>
