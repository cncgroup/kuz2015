<h2 id="ru">Registrácia účastníkov</h2>
<form enctype="multipart/form-data" action="" method="post" name="main">
	<input type="hidden" name="action" id="action" value="none"/>
<?php
	if (isset($_SESSION['newsub']) && $_SESSION['newsub'] > -1) {
?>
		<p class="message">
			Gratulujeme, práve ste sa úspešne zaregistrovali na konferenciu Kongícia a umelý život 2015.<br/>Vaše registračné číslo je <?=$_SESSION['newregid']?>.
		</p>
		<input type="button" class="button" value="Prihlásiť ďalšieho účastníka" name="clearsubmission" id="clearsubmission" onclick="clearSubmission()"/>
<?php		
	} else {
?>			
		<fieldset>
			<p class="message">
				<?=($message != "")?$message:"Polia vyznačené hrubým písmom sú povinné."?>
			</p>
<?php
		$lastValues = array();
//		/$peopleFields = array("id", "firstauthor", "email", "otherauthor", "title", "abstract", "stype", "notes");
	foreach ($peopleFields as $name) {
		if ($name == 'id' || $name == 'note' || $name == 'createdate') {
			;
		}
		elseif ($name == 'roomtype') {
			$val = (isset($_POST[$name])) ? $_POST[$name] : 1;
		?>			
			<label for="<?=$name?>"><?=$peopleFieldNames[$name]?>:</label>
			<select id="<?=$name?>" name="<?=$name?>">
				<option value="0"<?=($val==0)?' selected="selected"':''?>>Jednotka</option>
				<option value="1"<?=($val==1)?' selected="selected"':''?>>Dvojka</option>
			</select>
			<div class="clear"></div>
		<?php
		}
		elseif ($name == 'vegetarian' || $name == 'student') {
			$val = (isset($_POST[$name])) ? $_POST[$name] : 0;
		?>			
			<label for="<?=$name?>"><?=$peopleFieldNames[$name]?>:</label>
			<select id="<?=$name?>" name="<?=$name?>">
				<option value="0"<?=($val==0)?' selected="selected"':''?>>Nie</option>
				<option value="1"<?=($val==1)?' selected="selected"':''?>>Áno</option>
			</select>
			<div class="clear"></div>
		<?php
		}
		else {
			$val = (isset($_POST[$name])) ? $_POST[$name] : "";
			$label = $peopleFieldNames[$name];
			if (in_array($name,$peopleFieldsObligatory,true))
				$label = "<strong>".$label."</strong>";
			?>
			<label for="<?=$name?>"><?=$label?>:</label>
			<input type="text" name="<?=$name?>" id="<?=$name?>" value="<?=$val?>"/>
			<div class="clear"></div>
			<?php
		}
	}
?>
			<div class="captchabox">
				<label for="captchain"><strong>Captcha kontrola*:</strong></label>
				<input type="text" name="captchain" id="captchain" value=""/>
				<img src="<?=$_SESSION['captcha']['image_src']?>" alt="CAPTCHA" />
				<p>*pre dokončenie registrácie vyplňte napíšte do príslušného políčka znaky na obrázku.</p>
			</div>
			<div class="notesbox">
				<label for="notes">Poznámka:</label>
				<textarea id="note" name="note" rows="5" cols="30"><?=(isset($_POST['note'])?$_POST['note']:"")?></textarea>
				<div class="clear"></div>
				<p>*napíšte nám, ak máte ďalšie požiadavky</p>
			</div>
			<div class="clear"></div>
			<input type="button" class="button" value="Prihlásiť účastníka" name="submitbutton" id="submittbutton" onclick="addSumbission()"/> 
		</fieldset>
<?php } ?>		
</form>

