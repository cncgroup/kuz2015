<?php

function sendConfirMail($to,$vlozne,$regid) {
	$subject = "registrácia na KUZ 2015";
	$message = "Vážená pani kolegyňa, vážený pán kolega,"."\r\n\n".
			"ďakujeme za Vašu registráciu na konferenciu KUZ 2015. Platbu vložného uskutočnite prevodom."."\r\n\n".
			"suma: ".$vlozne." €"."\r\n".
            "Číslo účtu: 2000750724/8330 (v SR) - použiť pri platbách v SR (v EUR)"."\r\n".
            "Číslo účtu: 2000750724/2010 (v ČR) - použiť pri platbách z ČR (v CZK)"."\r\n".
            "Názov účtu: SSKV"."\r\n".
            "Majiteľ účtu: Slovenská spoločnosť pre kognitívnu vedu"."\r\n".
            "IČO: 42416591"."\r\n".
            "Adresa banky: Fio banka, a.s., pobočka zahr. banky, Nám. SNP 21, 811 01 Bratislava"."\r\n".
            "IBAN kód: SK53 8330 0000 0020 0075 0724 (pre iné meny a medzinár. platby)"."\r\n".
            "SWIFT/BIC kód: FIOZSKBAXXX"."\r\n".
			"variabilný symbol: ".$regid."\r\n\n".
			//"Váš príspevok v rozsahu 6 až 8 strán (odporúčame párny počet strán) očakávame do 31.03.2015. Prosíme posnažte sa o dodržanie tohto termínu"."\r\n\n".
			"Váš príspevok bude zaradený do zborníka, ak aspoň jeden autor príspevku sa zaregistruje na KUZ 2015 a uhradí vložné"."\r\n\n".
			"So srdečným pozdravom za organizačný výbor,"."\r\n".
			"Ján Rybár a Igor Farkaš";
	$headers = 'From: kuz2015@lists.dai.fmph.uniba.sk'."\r\n".
		'Reply-To: kuz2015@lists.dai.fmph.uniba.sk'."\r\n".
		"MIME-Version: 1.0"."\r\n".
		"Content-Type: text/plain;charset=utf-8"."\r\n".
		'X-Mailer: PHP/'.phpversion(); 
	mail($to, $subject, $message, $headers);
}

function sendConfirMailSubmiss($submissionData) {
	$to      = $submissionData['email'];
	$subject = "registrácia príspevku na KUZ 2015";
	$message = "<p>Vážená pani kolegyňa, pán kolega,</p>"."\r\n".
				"<p>Váš príspevok s názvom \"".$submissionData['title']."\" bol úspešne zaregistrovaný na konferenciu Kognícia a umelý život 2015, ".
				"ktorá sa bude konať v dňoch 25.–28.5.2015 v hoteli Flóra v Trenčianskych Tepliciach."."\r\n".
				"<p>S pozdravom,<br/>"."\r"."organizačný výbor KUZ 2015</p>";
	$headers = 'From: kuz2015@lists.dai.fmph.uniba.sk' . "\r\n" .
		'Reply-To: kuz2015@lists.dai.fmph.uniba.sk' . "\r\n" .
		"MIME-Version: 1.0" . "\r\n" .
		"Content-Type: text/html;charset=utf-8" . "\r\n" .
		'X-Mailer: PHP/' . phpversion(); 
	mail($to, $subject, $message, $headers);
}

function getString() {
	if (isset($_GET)) {
		$get = "";
		foreach ($_GET as $key => $value)
			$get = $get . $key . '=' . $value . '&amp;';
		return $get;
	} else {
		return null;
	}
}

function createNavi($mainTable) {
	$out = "";
	$q_navidata = "SELECT name,getname FROM " . $mainTable . " WHERE primarytext = '1' ORDER BY id ASC";
	$r_navidata = mysql_query($q_navidata);
	if ($r_navidata) {
		$out.= '<ul>' . "\n";
		while ($row = mysql_fetch_assoc($r_navidata)) {
			$out.= '<li><a href="' . $row['getname'] . '">' . $row['name'] . '</a></li>' . "\n";
		}
		$out.= '</ul>' . "\n";
		mysql_free_result($r_navidata);
	}	
	return $out;
}

function createNaviPriority($mainTable) {
        $out = "";
        $q_navidata = "SELECT name,getname FROM " . $mainTable . " WHERE primarytext = '1' ORDER BY priority ASC";
        $r_navidata = mysql_query($q_navidata);
        if ($r_navidata) {
                $out.= '<ul>' . "\n";
                while ($row = mysql_fetch_assoc($r_navidata)) {
					$out.= '<li><a href="' . $row['getname'] . '">' . $row['name'] . '</a></li>' . "\n";
                }
                $out.= '</ul>' . "\n";
                mysql_free_result($r_navidata);
        }
        return $out;
}

function createContent($mainTable,$getname) {
	$out = "";
	$q = "SELECT * FROM ".$mainTable." WHERE getname = '".mysql_real_escape_string($getname)."'";
	$r_mainbody = mysql_query($q);
    $filename = "home/".$getname.".phtml";
	if ($r_mainbody && mysql_num_rows($r_mainbody) > 0 && file_exists($filename)) {
		mysql_free_result($r_mainbody);
        require_once($filename);
	} else {
		$out = "<p>Pri generovaní stránky nastala chyba</p>"."\n";
		echo mysql_error();
	}
	return $out;
}

function createFooter($mainTable) {
	$out = "";
	$q = "SELECT * FROM " . $mainTable . " WHERE name = 'footer'";
	$r_footer = mysql_query($q);
	if ($r_footer) {
		$out.= mysql_result($r_footer, 0, "text")."\n";
		mysql_free_result($r_footer);
	}
	return $out;
}

function createGet($params) {
	$out = "";
	foreach ($params as $key => $val) {
		$out .= $key . "=" . $val . "&amp;";
	}
	$out = substr($out, 0, strlen($out) - 5);
	return $out;
}

function listDirFiles($dirPath) {
	$fileList = array();
	$counter = 1;
	if (file_exists($dirPath)) {
		if ($handle = opendir($dirPath)) {
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != "..") {
					if (is_dir("$dirPath/$file")) {
					} else {
						$fileList[$counter] = $file;
						$counter++;
					}
				}
			}
			closedir($handle);
		} else
			echo "error opening file";
	}
	return $fileList;
}
?>
