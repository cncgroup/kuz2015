<?php
if (isset($_POST['action'])) {
	if ($_POST['action'] == "add") {
		$valid = true;
		$valid = $valid && (isset($_SESSION['newsub']) && $_SESSION['newsub'] == -1);
		foreach ($peopleFieldsObligatory as $f) {
			$valid = $valid && (isset($_POST[$f]) && $_POST[$f] != "");
		}
		if (!$valid)
			$message = $message."Nevyplnili ste všetky povinné údaje.<br/>";
		if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $_POST['email'])) {
			$valid = false;
			$message = $message."Zadali ste neplatnú emailovú adresu.<br/>";
		}
		if (!isset($_POST['captchain']) || trim(strtolower($_POST['captchain'])) != strtolower($_SESSION['prevcaptcha']['code'])) {
			$valid = false;
			$message = $message."Zadali ste nesprávny captcha kód.<br/>";
		}
		if ($valid) {
			$query = "INSERT INTO ".$peopleTable." (" . implode(', ', $peopleFields) . ") VALUES (null";
			foreach ($peopleFields as $key) {
				//echo $key;
				if (isset($_POST[$key])) {
					if ($key == "id" || $key == "createdate")
						;
					else
						$query = $query.", '".mysql_real_escape_string(trim($_POST[$key]))."'";
				}
			}
			$query = $query . ", NOW())";
			//echo "<br/>".$query;
			if (mysql_query($query)) {
				$newid = mysql_insert_id();
				$_SESSION['newsub'] = $newid;
				$tmp = date('Ymd');
				$tmp .= ($newid>10)?"":"0";
				$tmp .= $newid;
				$_SESSION['newregid'] = $tmp;
				$vlozne = 0;
				if ($_POST['student'] == 1) {
					$vlozne = (date('Ymd') > date('Ymd', $feeThreshold)) ? $studentFee : $studentFeeEarly;
				}
				else {
					if (date('Ymd') > date('Ymd', $feeThreshold))
						$vlozne = ($_POST['roomtype'] == 0) ? $singleFee : $doubleFee;
					else
						$vlozne = ($_POST['roomtype'] == 0) ? $singleFeeEarly : $doubleFeeEarly;
				}
				sendConfirMail($_POST['email'],$vlozne,$tmp);
			} else {
				$message = $errordbupload.'<br/>' . mysql_error();
			}
		}
	}
	elseif ($_POST['action'] == "clear") {
		unset($_SESSION['newsub']);
	}
}
//	if ($_POST['action'] == "add") {
//		$valid = true;
//		$valid = $valid && (isset($_SESSION['newsub']) && $_SESSION['newsub'] == -1);
//		foreach ($submissionFieldsObligatory as $f) {
//			$valid = $valid && (isset($_POST[$f]) && $_POST[$f] != "");
//		}
//		if (!$valid)
//			$message = $message."Nevyplnili ste všetky povinné údaje.<br/>";
//		if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $_POST['email'])) {
//			$valid = false;
//			$message = $message."Zadali ste neplatnú emailovú adresu.<br/>";
//		}
//		if (!isset($_POST['captchain']) || trim(strtolower($_POST['captchain'])) != strtolower($_SESSION['prevcaptcha']['code'])) {
//			$valid = false;
//			$message = $message."Zadali ste nesprávny captcha kód.<br/>";
//		}
//		if ($valid) {
//			$query = "INSERT INTO ".$submissionTable." (" . implode(', ', $submissionFields) . ") VALUES (null";
//			foreach ($submissionFields as $key) {
//				//echo $key;
//				if (isset($_POST[$key])) {
//					if ($key == "id" || $key == "createdate")
//						;
//					else
//						$query = $query.", '".mysql_real_escape_string(trim($_POST[$key]))."'";
//				}
//			}
//			$query = $query . ", NOW())";
//			//echo "<br/>".$query;
//			if (mysql_query($query)) {
//				$newid = mysql_insert_id();
//				$_SESSION['newsub'] = $newid;
//                sendConfirMailSubmiss($_POST);
//			} else {
//				$message = $errordbupload.'<br/>' . mysql_error();
//			}
//		}
//	}
//	elseif ($_POST['action'] == "clear") {
//		unset($_SESSION['newsub']);
//	}
//}
?>
