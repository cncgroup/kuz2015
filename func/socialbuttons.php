<div id="social">

	<!--Facebook-->
	<div class="socbutton">
	<iframe 
		src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fii.fmph.uniba.sk%2Fmeicogsci%2F&amp;layout=button_count&amp;show_faces=false&amp;width=450&amp;action=like&amp;font=verdana&amp;colorscheme=light&amp;height=21" 
		scrolling="no" frameborder="0" style="border:none; overflow:hidden; 
		width:80px; height:21px;" 
		allowTransparency="true">
	</iframe>
	</div>
	<!--/Facebook-->
	
	<!--Twitter-->
	<script>
			!function(d,s,id){
			var js,fjs=d.getElementsByTagName(s)[0];
			if(!d.getElementById(id)){
				js=d.createElement(s);
				js.id=id;js.src="//platform.twitter.com/widgets.js";
				fjs.parentNode.insertBefore(js,fjs);
			}
		}(document,"script","twitter-wjs");
	</script>
	<div class="socbutton">
		<a href="https://twitter.com/share" class="twitter-share-button" data-dnt="true">Tweet</a>
	</div>
	<!--/Twitter-->

	<!--GPlusOne-->
	<script type="text/javascript">
	(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		po.src = 'https://apis.google.com/js/plusone.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	})();
	</script>
	<div class="socbutton">
		<g:plusone annotation="inline" size="medium" width="120"></g:plusone>
	</div>
	<!--/GPlusOne-->
	
	<div class="clear"></div>
</div>