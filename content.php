<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="sk">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="Konferencia kognícia a umelý život"/>
    <meta name="keywords" content="kognitívna veda, umelá inteligencia, konferencia, psychológia, filozofia, neuroveda, informatika"/>

    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link rel="stylesheet" href="css/kuzstyle.css" type="text/css" media="screen"/>

    <link rel="stylesheet" type="text/css" href="func/uberGallery/UberGallery.css" />
    <link rel="stylesheet" type="text/css" href="func/uberGallery/themes/uber-custom/rebase-min.css" />
    <link rel="stylesheet" type="text/css" href="func/uberGallery/themes/uber-custom/style.css" />
    <link rel="stylesheet" type="text/css" href="func/uberGallery/colorbox/1/colorbox.css" />

    <script src="js/scripts.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="func/uberGallery/colorbox/jquery.colorbox.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("a[rel='colorbox']").colorbox({maxWidth: "90%", maxHeight: "90%", opacity: ".5"});
        });
    </script>

    <title>Kognícia a umelý život 2015</title>
</head>
<body>
<div id="wrap">
    <div id="header">
        <h1>Kognícia a umelý život 2015</h1>
        <p>Hotel Flóra, Trenčianske Teplice, 25.–28. 5. 2015</p>
    </div>
    <div id="navi">
        <?php echo createNaviPriority($mainTable); ?>
        <div class="clear"></div>
    </div>
    <div id="contentwrap">
        <div id="content">
            <?php
            $getname = "domov";
            if (isset($_GET['action'])) {
                $getname = $_GET['action'];
            }
//            if ($getname == "registracia") {
//                include "func/regFormPeople.php";
//            }
//            else
            if ($getname == $submissions) 
            {
                include "func/submissions.php";
            }
            elseif ($getname == $people) 
            {
                include "func/people.php";
            } 
            elseif ($getname == $gallery) 
            {
				include "func/gallery.php";
				
            } else 
            {
                echo createContent($mainTable, $getname);
            }
            ?>
        </div>
        <div id="panel_right">
            <?php echo createContent($mainTable, "panelright");?>
        </div>
        <div class="clear"></div>
    </div>
    <div id="footer">
        <p>Konferencia "Kognícia a umelý život 2015", Centrum pre kognitívnu vedu, KAI FMFI UK, Bratislava ©2014-2015</p>
    </div>
</div>
</body>
</html>
