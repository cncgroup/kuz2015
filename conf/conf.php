<?php
$mainTable = "home";
$userTable = "users";

$submissionTable = "submissions_final";

$submissionFields = array("id", "firstauthor", "email", "otherauthor", "title", "abstract", "notes", "createdate");
$submissionFieldsObligatory = array("firstauthor", "email", "title", "abstract");
$submissionFieldNames = array(
	"id"			=> "Id", 
	"firstauthor"	=> "Prvý autor", 
	"email"			=> "Kontaktný email",
	"otherauthor"	=> "Ďalší autori",
	"title"			=> "Názov príspevku",
	"abstract"		=> "Abstrakt",
	"stype"			=> "Typ príspevku",
	"notes"			=> "Poznámky",
);

$peopleTable = "people";

$peopleFields = array("id", "name", "surname", "titles", "university", "department", "street", "city", "postalcode", "state", 
						"telephone", "fax", "email", "web", "roomtype", "roomwith", "student", "vegetarian", "note", "createdate");
$peopleTinyFields = array("id","roomtype","vegetarian","student");
$peopleFieldsObligatory = array("name", "surname", "university", "department", "city", "postalcode", "state", "email");
$peopleFieldNames = array(
	"id"		=> "Id",
	"name"		=> "Meno",
	"surname"	=> "Priezvisko",
	"titles"	=> "Tituly",
	"university"=> "Univerzita",
	"department"=> "Pracovisko",
	"street"	=> "Ulica",
	"city"		=> "Mesto",
	"postalcode"=> "PSČ",
	"state"		=> "Štát", 
	"telephone"	=> "Telefón",
	"fax"		=> "Fax",
	"email"		=> "Email",
	"web"		=> "Web",
	"roomtype"	=> "Typ izby",
	"roomwith"	=> "Spolubývajúci/a",
	"student"	=> "Som študent/ka",
	"vegetarian"=> "Som vegetarián/ka",
	"note"		=> "Poznámka",
	"createdate"=> "Dátum",
);
//GENERIC TEXTS
$errormessage = "<br/><p>An error ocurred during page generation.</p>"."\n";
$errorlogin = "Wrong username / password."."\n";
$errordatabase = '<p class="warning">Unable to establish connection with the database</p>'."\n";
$errordbupload = "Nastala chyba pri ukladaní do databázy";
$errormessagesk = "<p>Pri generovaní stránky nastala chyba</p>"."\n";

$captchaParams = array(
    'min_length' => 4,
    'max_length' => 4,
    'min_font_size' => 20,
    'max_font_size' => 30,
    'color' => '#384d05',
    'angle_min' => 0,
    'angle_max' => 50,
    'shadow' => true,
    'shadow_color' => '#fff',
    'shadow_offset_x' => -1,
    'shadow_offset_y' => 1,
);

$feeThreshold = mktime(0, 0, 0, 4, 10, 2015);
$singleFeeEarly = 290;
$singleFee = 320;
$doubleFeeEarly = 255;
$doubleFee = 280;
$studentFeeEarly = 230;
$studentFee = 250;

$submissions = "prispevky";
$people = "ucastnici";
$poepleLinkName = "ucastnik";
$gallery = "fotogaleria";
?>
