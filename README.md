# README #

This is the webpage of KUZ 2015 conference.

# DB QUERIES #

## people ##
select
p.name,p.surname,p.university,p.state,p.email,p.roomtype,p.roomwith,p.vegetarian,p.student,p.note,s.firstauthor,s.otherauthor,s.title
from people p
left join submissions s
on (s.firstauthor like concat('%',p.surname,'%') or s.otherauthor like concat('%',p.surname,'%'));

## submissionless entries ##
select firstauthor,title from submissions where title like '%bez prispevku%' or title like '-'
select * from people where concat('name',' ','surname') in (select firstauthor from submissions where title like '%bez prispevku%' or title like '-');

## submissions probably without people ##
select s.id,s.title,s.firstauthor,s.createdate from submissions s where s.firstauthor not in (select concat(name,' ',surname) from people);